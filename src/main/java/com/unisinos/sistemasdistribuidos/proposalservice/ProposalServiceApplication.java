package com.unisinos.sistemasdistribuidos.proposalservice;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableRabbit
@SpringBootApplication
public class ProposalServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProposalServiceApplication.class, args);
	}

}
