package com.unisinos.sistemasdistribuidos.proposalservice.impl;

import com.unisinos.sistemasdistribuidos.proposalservice.api.domain.*;
import com.unisinos.sistemasdistribuidos.proposalservice.api.repository.*;
import com.unisinos.sistemasdistribuidos.proposalservice.api.service.ProposalService;
import com.unisinos.sistemasdistribuidos.proposalservice.broker.NotificationProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ProposalServiceImpl implements ProposalService {

	private ConsultantRepository consultantRepository;

	private ClientRepository clientRepository;

	private LoanRepository loanRepository;

	private ProposalRepository proposalRepository;

	private NotificationProducer notificationProducer;

	@Autowired
	public ProposalServiceImpl(ConsultantRepository consultantRepository,
	                           ClientRepository clientRepository,
	                           LoanRepository loanRepository,
	                           ProposalRepository proposalRepository,
	                           NotificationProducer notificationProducer) {
		this.consultantRepository = consultantRepository;
		this.clientRepository = clientRepository;
		this.loanRepository = loanRepository;
		this.proposalRepository = proposalRepository;
		this.notificationProducer = notificationProducer;
	}

	@Override
	public Proposal create(Proposal proposal) {
		proposal.setCreatedAt(LocalDateTime.now());

		Loan loan = loanRepository.findById(proposal.getLoanId()).orElse(null);
		proposal.setLoan(loan);

		Client client = clientRepository.findById(proposal.getClientId()).orElse(null);
		proposal.setClient(client);

		Consultant consultant = consultantRepository.findById(proposal.getConsultantId()).orElse(null);
		proposal.setConsultant(consultant);

		proposal.setProposalStatus(ProposalStatus.WAITING_VALIDATION);
		proposal = proposalRepository.save(proposal);

		sendNotificationAsync(proposal);

		return proposal;
	}

	@Async
	public void sendNotificationAsync(Proposal proposal) {
		notificationProducer.send(proposal);
	}
}