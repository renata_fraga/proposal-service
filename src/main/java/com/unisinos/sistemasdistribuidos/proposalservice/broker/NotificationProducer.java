package com.unisinos.sistemasdistribuidos.proposalservice.broker;

import com.unisinos.sistemasdistribuidos.proposalservice.api.domain.Proposal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class NotificationProducer {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private ConverterProposalEntityToNotificationMessage converterProposalEntityToNotificationMessage;

	private RabbitTemplate rabbitTemplate;

	@Value("${rabbitmq.exchanges.notification-dx}")
	private String exchange;

	@Value("${rabbitmq.routings.notification-create}")
	private String routingKey;

	@Autowired
	public NotificationProducer(ConverterProposalEntityToNotificationMessage converterProposalEntityToNotificationMessage,
	                            RabbitTemplate rabbitTemplate) {
		this.converterProposalEntityToNotificationMessage = converterProposalEntityToNotificationMessage;
		this.rabbitTemplate = rabbitTemplate;
	}

	public void send(Proposal proposal) {
		NotificationMessage notificationMessage = converterProposalEntityToNotificationMessage
				.converter(proposal);
		log.info("NotificationProducer: send -> params: notificationMessage - {} ", notificationMessage);
		rabbitTemplate.send(exchange, routingKey, setupMessage(notificationMessage));
	}

	private Message setupMessage(NotificationMessage notificationMessage) {
		Message message = rabbitTemplate.getMessageConverter().toMessage(notificationMessage, new MessageProperties());
		message.getMessageProperties().getHeaders().put("__TypeId__", "NotificationMessage");
		message.getMessageProperties().setContentType(MediaType.APPLICATION_JSON_VALUE);
		return message;
	}
}