package com.unisinos.sistemasdistribuidos.proposalservice.broker;

import com.unisinos.sistemasdistribuidos.proposalservice.api.domain.Proposal;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class ConverterProposalEntityToNotificationMessage {

	public NotificationMessage converter(Proposal proposal) {
		NotificationMessage notificationMessage = new NotificationMessage();
		notificationMessage.setTitle("Created Proposal");
		notificationMessage.setDescription(createDescription(proposal));
		notificationMessage.setPlatformType("MOBILE");
		notificationMessage.setPublisher(proposal.getClient().getDocument());

		Map<String, Object> additionalParams = new LinkedHashMap<>();
		additionalParams.put("metaId", proposal.getId());

		notificationMessage.setAdditionalParams(additionalParams);
		return notificationMessage;
	}

	private String createDescription(Proposal proposal) {
		return new StringBuilder().append("Olá ").append(proposal.getClient().getFullName())
				.append(",")
				.append("Você acabou de ter sua proposa criada pelo consultor ")
				.append(proposal.getConsultant().getFullName())
				.append("!")
				.append("Agora você precisa aguardar as próximas instruções.")
				.append("O protocolo da sua solicitação é ")
				.append(proposal.getId()).toString();
	}
}
