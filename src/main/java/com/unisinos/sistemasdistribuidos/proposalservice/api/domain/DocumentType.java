package com.unisinos.sistemasdistribuidos.proposalservice.api.domain;


public enum DocumentType {
	CPF,
	RG
}
