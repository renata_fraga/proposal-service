package com.unisinos.sistemasdistribuidos.proposalservice.api.repository;

import com.unisinos.sistemasdistribuidos.proposalservice.api.domain.Loan;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LoanRepository extends PagingAndSortingRepository<Loan, Long> {
}
