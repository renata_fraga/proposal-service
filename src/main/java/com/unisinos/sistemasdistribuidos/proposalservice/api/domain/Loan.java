package com.unisinos.sistemasdistribuidos.proposalservice.api.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class Loan implements Serializable {

	private static final long serialVersionUID = 6352015074433981956L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	private String tableType;

	private LocalDateTime deadline;

	private LocalDateTime emission;

	private LocalDateTime firstDueDate;

	private BigDecimal amount;

	private BigDecimal IOFValue;

	private BigDecimal monthRate;

	private BigDecimal yearRate;

	private boolean viability;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTableType() {
		return tableType;
	}

	public void setTableType(String tableType) {
		this.tableType = tableType;
	}

	public LocalDateTime getDeadline() {
		return deadline;
	}

	public void setDeadline(LocalDateTime deadline) {
		this.deadline = deadline;
	}

	public LocalDateTime getEmission() {
		return emission;
	}

	public void setEmission(LocalDateTime emission) {
		this.emission = emission;
	}

	public LocalDateTime getFirstDueDate() {
		return firstDueDate;
	}

	public void setFirstDueDate(LocalDateTime firstDueDate) {
		this.firstDueDate = firstDueDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getIOFValue() {
		return IOFValue;
	}

	public void setIOFValue(BigDecimal IOFValue) {
		this.IOFValue = IOFValue;
	}

	public BigDecimal getMonthRate() {
		return monthRate;
	}

	public void setMonthRate(BigDecimal monthRate) {
		this.monthRate = monthRate;
	}

	public BigDecimal getYearRate() {
		return yearRate;
	}

	public void setYearRate(BigDecimal yearRate) {
		this.yearRate = yearRate;
	}

	public boolean isViability() {
		return viability;
	}

	public void setViability(boolean viability) {
		this.viability = viability;
	}
}
