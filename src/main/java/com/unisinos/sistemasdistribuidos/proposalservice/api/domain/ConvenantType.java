package com.unisinos.sistemasdistribuidos.proposalservice.api.domain;

public enum ConvenantType {
	INSS,
	SIAPE;
}
