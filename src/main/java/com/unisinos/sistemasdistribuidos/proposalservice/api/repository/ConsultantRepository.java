package com.unisinos.sistemasdistribuidos.proposalservice.api.repository;

import com.unisinos.sistemasdistribuidos.proposalservice.api.domain.Consultant;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ConsultantRepository extends PagingAndSortingRepository<Consultant, Long> {
}
