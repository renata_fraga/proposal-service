package com.unisinos.sistemasdistribuidos.proposalservice.api.repository;

import com.unisinos.sistemasdistribuidos.proposalservice.api.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {
}
