package com.unisinos.sistemasdistribuidos.proposalservice.api.domain;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Consultant extends User {

	private String registration;

	private LocalDateTime dateOfRegistration;

	private String office;

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public LocalDateTime getDateOfRegistration() {
		return dateOfRegistration;
	}

	public void setDateOfRegistration(LocalDateTime dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}

	public String getOffice() {
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}
}
