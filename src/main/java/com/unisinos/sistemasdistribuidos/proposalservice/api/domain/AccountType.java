package com.unisinos.sistemasdistribuidos.proposalservice.api.domain;

public enum AccountType {
	CORRENTE,
	POUPANCA;
}
