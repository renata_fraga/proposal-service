package com.unisinos.sistemasdistribuidos.proposalservice.api.domain;

import javax.persistence.*;

@Entity
public class Client extends User {

	private String nationality;

	private String degreeEducation;

	@Enumerated(EnumType.STRING)
	private AccountType accountType;

	private String accountNumber;

	private String accountDigit;

	@ManyToOne
	@JoinColumn(name = "bank_id")
	private BankAccount bankAccount;

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getDegreeEducation() {
		return degreeEducation;
	}

	public void setDegreeEducation(String degreeEducation) {
		this.degreeEducation = degreeEducation;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountDigit() {
		return accountDigit;
	}

	public void setAccountDigit(String accountDigit) {
		this.accountDigit = accountDigit;
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}
}
