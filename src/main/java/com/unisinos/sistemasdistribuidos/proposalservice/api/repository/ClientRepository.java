package com.unisinos.sistemasdistribuidos.proposalservice.api.repository;

import com.unisinos.sistemasdistribuidos.proposalservice.api.domain.Client;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {
}
