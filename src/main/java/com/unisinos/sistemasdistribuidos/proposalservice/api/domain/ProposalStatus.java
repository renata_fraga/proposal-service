package com.unisinos.sistemasdistribuidos.proposalservice.api.domain;

public enum ProposalStatus {
	WAITING_VALIDATION(10),
	PENDING_AUTOMATIC(20),
	PENDING_DOCUMENTATION(30),
	SEND_CONVENANT(50),
	WAITING_PAYMENT(60),
	APPROVED(100),
	REFUSED(21),
	REFUSED_CONVENT(51),
	CANCEL(101);

	private final int value;

	private ProposalStatus(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
