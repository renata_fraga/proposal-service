package com.unisinos.sistemasdistribuidos.proposalservice.api.repository;

import com.unisinos.sistemasdistribuidos.proposalservice.api.domain.Proposal;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProposalRepository extends PagingAndSortingRepository<Proposal, Long> {
}
