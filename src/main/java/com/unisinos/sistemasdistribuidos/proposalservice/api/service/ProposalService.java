package com.unisinos.sistemasdistribuidos.proposalservice.api.service;

import com.unisinos.sistemasdistribuidos.proposalservice.api.domain.Proposal;

public interface ProposalService {

	Proposal create(Proposal proposal);

}
