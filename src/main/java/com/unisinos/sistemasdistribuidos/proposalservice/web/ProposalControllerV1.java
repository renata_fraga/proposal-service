package com.unisinos.sistemasdistribuidos.proposalservice.web;

import com.unisinos.sistemasdistribuidos.proposalservice.api.domain.Proposal;
import com.unisinos.sistemasdistribuidos.proposalservice.api.service.ProposalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/v1/proposal", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProposalControllerV1 {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProposalService proposalService;

	@Autowired
	private ConverterProposalDtoToEntity converterProposalDtoToEntity;

	@PostMapping
	public ResponseEntity<Object> create(@RequestBody ProposalDto proposalDto) {
		log.info("ProposalControllerV1: create -> params: proposalDto - {} ", proposalDto);
		Proposal proposal = proposalService.create(converterProposalDtoToEntity.converter(proposalDto));
		Map<String, Object> response = new LinkedHashMap<>();
		response.put("id", proposal.getId());
		log.info("ProposalControllerV1: create -> response: httpStatus = {} ", HttpStatus.CREATED);
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}
}
