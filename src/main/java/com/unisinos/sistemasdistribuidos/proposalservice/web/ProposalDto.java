package com.unisinos.sistemasdistribuidos.proposalservice.web;

import java.io.Serializable;

public class ProposalDto implements Serializable {

	private static final long serialVersionUID = 8579640749707914069L;

	private Long clientId;

	private Long consultantId;

	private Long loanId;

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getConsultantId() {
		return consultantId;
	}

	public void setConsultantId(Long consultantId) {
		this.consultantId = consultantId;
	}

	public Long getLoanId() {
		return loanId;
	}

	public void setLoanId(Long loanId) {
		this.loanId = loanId;
	}

	@Override
	public String toString() {
		return "ProposalDto{" +
				"clientId=" + clientId +
				", consultantId=" + consultantId +
				", loanId=" + loanId +
				'}';
	}
}
