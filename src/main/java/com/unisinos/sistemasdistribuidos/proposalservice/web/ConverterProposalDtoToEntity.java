package com.unisinos.sistemasdistribuidos.proposalservice.web;

import com.unisinos.sistemasdistribuidos.proposalservice.api.domain.Proposal;
import org.springframework.stereotype.Component;

@Component
public class ConverterProposalDtoToEntity {

	public Proposal converter(ProposalDto proposalDto) {
		Proposal proposal = new Proposal();
		proposal.setClientId(proposalDto.getClientId());
		proposal.setConsultantId(proposalDto.getConsultantId());
		proposal.setLoanId(proposalDto.getLoanId());
		return proposal;
	}
}
